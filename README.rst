steptools
=========

steptools is a collection of tools for working with values in "steps".
This includes enumerating a range of steps and finding the previous and
next step when you have a value that's not directly on a step.

It includes:

steptools.range: a drop-in replaceent for range adding with
support for anything that behaves reasonably like a number, including
floats, datetime.datetime, datetime.date, fractions.Fraction, and
decimal.Decimal.


#! /usr/bin/python3

""" rangetools.py

MIT License

Copyright (c) 2021 Alan De Smet

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
# SPDX-License-Identifier: MIT

from steptools import range

import unittest

class ImperialLength:
    """ Bare minimum example that can be used with range

    The same class can be used for stop, stop, and step.

    Because __init__ requires an argument (feet), zero_step and start cannot be
    automatically created and must be passed in.
    """
    def __init__(self, feet, miles=0):
        self.feet = feet % 5280
        self.miles = miles + (feet // 5280)

    def __add__(self, other):
        return ImperialLength(self.feet+other.feet, self.miles+other.miles)

    def __lt__(self, other):
        return (self.miles, self.feet) < (other.miles, other.feet)

    # __repr__ is NOT required by range, but is here to
    # generate better errors when tests fail.
    def __repr__(self):
        return f"ImperialLength({self.feet}, {self.miles})"


class rangeTester(unittest.TestCase):

    def assertList(self, alist, blist, inner_type=None):
        alist = list(alist)
        blist = list(blist)
        err = AssertionError(f"{alist} != {blist}")
        if len(alist) != len(blist): raise err
        for a,b in zip(alist,blist):
            if inner_type is not None:
                b = inner_type(b)
            if a != b or type(a) != type(b): raise err


    def basic_tests(self, result_type):
        def _assert(a,b):
            self.assertList(a,b,result_type)
        t=result_type
        _assert( range(t(3)                           ), [0,1,2]   )
        _assert( range(t(3),            inclusive=True), [0,1,2,3] )
        _assert( range(t(3),t( 5)                     ), [3,4]     )
        _assert( range(t(3),t( 5),      inclusive=True), [3,4,5]   )
        _assert( range(t(5),t( 3)                     ), []        )
        _assert( range(t(5),t( 3),t(-1)               ), [5,4]     )
        _assert( range(t(9),t( 3),t(-2)               ), [9,7,5]   )
        _assert( range(t(3),t(10),t(2)                ), [3,5,7,9] )
        _assert( range(t(3),t(10),t(2), inclusive=True), [3,5,7,9] )
        _assert( range(t(-3)                          ), []        )
        _assert( range(t(-3),t(-1)                    ), [-3,-2]   )
        _assert( range(t(-1),t(-5),t(-2)              ), [-1,-3]   )
        _assert( range(t(2),t(-2),t(-3)               ), [2,-1]    )

    def test_int(self):
        self.basic_tests(int)

    def test_float(self):
        self.basic_tests(float)
        # These tests are suspicious and should probably
        # explicitly allow for floating point inaccuracy.
        self.assertList( range(.1, 1.1), [.1])
        self.assertList( range(.1, 1.1, inclusive=True), [.1,1.1])
        self.assertList( range(.1, 1.3, .3), [.1,.4,.7,1.0])
        self.assertList( range(.2, -.4, -.2), [0.2,0.0,-0.2])

    def test_datetime(self):
        from datetime import datetime as dt, timedelta as delt
        self.assertList(
                range(dt(2019,12,30), dt(2020,1,1), delt(days=1)),
                [dt(2019,12,30),dt(2019,12,31)])
        self.assertList(
                range(dt(2020,1,1), dt(2019,12,30), delt(days=-1)),
                [ dt(2020,1,1), dt(2019,12,31)])
        self.assertList(
                range(dt(2020,1,1), dt(2019,12,30), delt(days=1)),
                [])
        self.assertList(
                range(dt(2019,12,30), dt(2020,1,1), delt(days=1), inclusive=True),
                [dt(2019,12,30),dt(2019,12,31),dt(2020,1,1)])
        self.assertList(
                range(dt(2020,6,6,3,30,10), dt(2020,6,6,3,31,40), delt(seconds=30)),
                [dt(2020, 6, 6, 3, 30, 10), dt(2020, 6, 6, 3, 30, 40), dt(2020, 6, 6, 3, 31, 10)])

    def test_date(self):
        from datetime import timedelta as delt, date
        self.assertList(
                range(date(2019,12,30), date(2020,1,1), delt(days=1)),
                [date(2019,12,30),date(2019,12,31)])
        self.assertList(
                range(date(2019,12,30), date(2020,1,1), delt(days=1), inclusive=True),
                [date(2019,12,30),date(2019,12,31),date(2020,1,1)])

    def test_fractions(self):
        from fractions import Fraction as F
        self.assertList( range(F(3,1)), [F(0),F(1),F(2)])
        self.assertList( range(F(1,3), F(3,3), F(1,3)), [F(1,3),F(2,3)])
        self.assertList( range(F(7,3), F(4,3), F(-1,3)), [F(7,3),F(6,3),F(5,3)])
        self.assertList( range(F(1,3), F(-4,9), F(-1,3)), [F(1,3),F(0),F(-1,3)])

    def test_decimal(self):
        from decimal import Decimal as D
        self.assertList( range(D(3)), [D(0),D(1),D(2)])
        self.assertList( range(D('3'), D('3.1492'), D('0.05')), [D('3.00'),D('3.05'),D('3.10')])
        self.assertList( range(D('1.2'), D('-1.1'), D('-0.8')), [D('1.2'),D('0.4'),D('-0.4')])

    def test_str(self):
        # This is deeply silly, but it should work
        self.assertList(range("x","xAAA","A"),["x", "xA", "xAA"])

    def test_custom_class(self):
        def assertList(alist,blist):
            alist = list(alist)
            err = AssertionError(f"{alist} != {blist}")
            if len(alist) != len(blist): raise err
            for a,b in zip(alist,blist):
                # To keep ImperialDist a minimal example, it doesn't
                # implement !=, so we fake it with <
                if a<b or b<a: raise err

        dist = ImperialLength
        assertList(
                range(dist(0), dist(12), step=dist(3), zero_step=dist(0)),
                [dist(0),dist(3),dist(6),dist(9)])
        assertList(
                range(dist(1000,2), dist(0, 4), step=dist(3000), zero_step=dist(0)),
                [dist(1000,2),dist(4000,2),dist(1720,3),dist(4720,3)])

    def test_errors(self):
        from datetime import datetime, timedelta
        with self.assertRaisesRegex(TypeError, 'Unable to add start.*to step'):
            list(range(""))
        with self.assertRaisesRegex(TypeError, 'Unable to add start.*to step'):
            list(range(datetime(2020,1,1),datetime(2020,1,2)))
        with self.assertRaisesRegex(TypeError, 'Unable to construct zero of type'):
            list(range(datetime(2020,1,1)))
        with self.assertRaisesRegex(ValueError, 'Unable to compare step.*to zero_step'):
            list(range(1,2,3,zero_step=""))
        with self.assertRaisesRegex(ValueError, r'range\(\) arg 3 \(step\) must not be zero'):
            list(range(datetime(2020,1,1),datetime(2020,1,2), timedelta()))

        dist = ImperialLength
        # Relying on automatic start, but it's not constructable with type(start)()
        with self.assertRaisesRegex(TypeError, 'Unable to construct zero of type'):
            list(range(dist(12)))

        # Relying on automatic step, but it's 1, which is not compatible
        with self.assertRaisesRegex(TypeError, 'Unable to add start .* to step.*must be compatible'):
            list(range(dist(0),dist(12)))
        # Relying on automtic zero_step, but it's not constructable with type(step)()
        with self.assertRaisesRegex(TypeError, 'Unable to contruct zero of the step type'):
            list(range(dist(0),dist(12),dist(1)))

def load_tests(loader, tests, ignore):
    """ Ensure unittest runs doctest

    You can run the tests with:

    python3 rangetools.py
    """
    import doctest
    import steptools
    tests.addTests(doctest.DocTestSuite(steptools))
    return tests


if __name__ == '__main__':
    unittest.main()
